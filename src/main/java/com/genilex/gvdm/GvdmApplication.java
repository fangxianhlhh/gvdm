package com.genilex.gvdm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GvdmApplication {

	public static void main(String[] args) {
		SpringApplication.run(GvdmApplication.class, args);
	}
}
