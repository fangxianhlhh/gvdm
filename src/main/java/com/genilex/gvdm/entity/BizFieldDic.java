package com.genilex.gvdm.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
@TableName("d_field")
//精友字段
public class BizFieldDic extends Model<BizFieldDic>{

    private Integer id;
    @TableField(value = "tab_id")
    private Integer tabId;
    @TableField(exist = false)
    private String tabName;
    private String code;
    private String name;
    private String remark;
    @TableField(value = "condition_state")
    private String conditionState;
    @TableField(value = "order_num")
    private String orderNum;

    @Override
    protected Serializable pkVal() {
        return id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTabId() {
        return tabId;
    }

    public void setTabId(Integer tabId) {
        this.tabId = tabId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getConditionState() {
        return conditionState;
    }

    public void setConditionState(String conditionState) {
        this.conditionState = conditionState;
    }

    public String getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
    }

    public String getTabName() {
        return tabName;
    }

    public void setTabName(String tabName) {
        this.tabName = tabName;
    }

    @Override
    public String toString() {
        return "BizFieldDic{" +
                "id=" + id +
                ", tabId=" + tabId +
                ", tabName='" + tabName + '\'' +
                ", code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", remark='" + remark + '\'' +
                ", conditionState='" + conditionState + '\'' +
                ", orderNum='" + orderNum + '\'' +
                '}';
    }
}
