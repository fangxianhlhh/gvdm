package com.genilex.gvdm.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

@TableName("b_cus_tab_field_info")
public class BizField extends Model<BizCustomer> {

	private Integer id;
	@TableField(value = "table_id")
	private String tableId;
	@TableField(value = "cus_field")
	private String cusField;
	@TableField(value = "local_field")
	private String localField;
	private String remark;
	@TableField(value = "crate_by")
	private String crateBy;
	@TableField(value = "crate_date")
	private Date crateDate;
	@TableField(value = "modified_by")
	private String modifiedBy;
	@TableField(value = "modified_date")
	private Date modifiedDate;

	@Override
	protected Serializable pkVal() {
		return id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTableId() {
		return tableId;
	}

	public void setTableId(String tableId) {
		this.tableId = tableId;
	}

	public String getCusField() {
		return cusField;
	}

	public void setCusField(String cusField) {
		this.cusField = cusField;
	}

	public String getLocalField() {
		return localField;
	}

	public void setLocalField(String localField) {
		this.localField = localField;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getCrateBy() {
		return crateBy;
	}

	public void setCrateBy(String crateBy) {
		this.crateBy = crateBy;
	}

	public Date getCrateDate() {
		return crateDate;
	}

	public void setCrateDate(Date crateDate) {
		this.crateDate = crateDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
}
