package com.genilex.gvdm.service;

import com.baomidou.mybatisplus.service.IService;
import com.genilex.gvdm.entity.BizCustomer;
import com.genilex.gvdm.entity.BizField;
import com.genilex.gvdm.entity.BizFieldDic;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 */
public interface IBizFieldService extends IService<BizField> {
}
