package com.genilex.gvdm.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.genilex.gvdm.entity.BizTable;
import com.genilex.gvdm.entity.BizTableDic;
import com.genilex.gvdm.mapper.BizTableDicMapper;
import com.genilex.gvdm.mapper.BizTableMapper;
import com.genilex.gvdm.service.IBizTableDicService;
import com.genilex.gvdm.service.IBizTableService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 */
@Service
public class BizTableDicServiceImpl extends ServiceImpl<BizTableDicMapper, BizTableDic> implements IBizTableDicService {

}
