package com.genilex.gvdm.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.genilex.gvdm.entity.BizCustomer;
import com.genilex.gvdm.entity.BizTable;
import com.genilex.gvdm.mapper.BizCustomerMapper;
import com.genilex.gvdm.mapper.BizTableMapper;
import com.genilex.gvdm.service.IBizCustomerService;
import com.genilex.gvdm.service.IBizTableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 */
@Service
public class BizTableServiceImpl extends ServiceImpl<BizTableMapper, BizTable> implements IBizTableService {

    @Autowired
    private BizTableMapper mapper;
    @Override
    public List<BizTable> selectListData(Map<String, Object> map) {
        return mapper.selectListData(map);
    }
}
