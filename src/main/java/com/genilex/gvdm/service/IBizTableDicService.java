package com.genilex.gvdm.service;

import com.baomidou.mybatisplus.service.IService;
import com.genilex.gvdm.entity.BizFieldDic;
import com.genilex.gvdm.entity.BizTable;
import com.genilex.gvdm.entity.BizTableDic;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lengleng
 * @since 2017-08-19
 */
public interface IBizTableDicService extends IService<BizTableDic> {
}
